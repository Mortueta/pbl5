import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * Clase HiloRecibir
 * 
 * @author Xabier Legarda
 * 
 * @brief Un hilo recibe los datos que el equipo de simulación le manda al equipo central.
 * @link CalculadorRuta
 * @link Buffer
 * @link Principal
 * @link EnviarNuevaRuta
 * 
 * @date 22/04/2014
 */

public class HiloRecibir extends Thread{
	private BufferedInputStream bis = null;
	private ObjectInputStream ois = null;
	private Integer numTren = 0;	
	private CalculadorRuta calculadorRuta = null;
	private Buffer buffer = null;
	private Principal principal = null;
	
	public HiloRecibir(ObjectInputStream ois,CalculadorRuta calculadorRuta,Buffer buffer,
					   Principal principal){
		this.ois=ois;
		this.calculadorRuta=calculadorRuta;
		this.buffer=buffer;
		this.principal=principal;
		
	}
	
	public void run(){
		/** 
		 * @brief función que define las acciones que debe realizar el hilo.
		 * @author Xabi
		 * @date 22/04/2014
		 * @version V1.0		 
		 * */

		recibirDatos();
	}
	public void recibirDatos() {
		/** 
		 * @brief recibe los datos que el equipo de simulación le manda al equipo central.
		 * @author Xabi
		 * @date 22/04/2014
		 * @version V1.0		 
		 * */
		int destino = 0;
		Via via = null;
		ArrayList<Via> nuevaRuta = null;
		int []rutaNuevaRealizar = null;
		EnviarNuevaRuta enviarNuevaRuta = null;
		boolean continuar = true;
		String seguimiento = null;
		
		while(continuar){
			ObjetoEnviar ob;
			try {
				try{	
					ob=(ObjetoEnviar) ois.readObject();
				}catch(EOFException e){
					principal.cerrarConexion();
					RegistroFallos.registrarFallos(e.getMessage());
					break;
				}
				seguimiento = "Numero de tren recibido: "+ob.getTrenId()+"\nVia recogida: " +
							  ""+ob.getViaAnterior();
				RegistroFallos.registrarSeguimiento(seguimiento);
				
				calculadorRuta.reestablecer();
				destino=calculadorRuta.getDestino(ob.getTrenId());
				via=calculadorRuta.getVia(ob.getViaAnterior());
				calculadorRuta.algoritmoBusqueda(via, destino);
				nuevaRuta=calculadorRuta.getListaCerrada();
				
				rutaNuevaRealizar=new int[nuevaRuta.size()];
				for(int i=0;i<nuevaRuta.size();i++){
					rutaNuevaRealizar[i]=nuevaRuta.get(i).getId();
				}
				enviarNuevaRuta=new EnviarNuevaRuta(ob.getTrenId(),rutaNuevaRealizar);
				buffer.establecerEnviarNuevaRuta(enviarNuevaRuta);
				buffer.liberar();
			} catch (ClassNotFoundException e) {
				RegistroFallos.registrarFallos(e.getMessage());
			} catch (IOException e) {
				RegistroFallos.registrarFallos(e.getMessage());
			}
		}
	}
	public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException{
		/** 
		 * @brief Se deserializa el objeto que se recibe.
		 * @param recibe de entrada los bytes que debe deserializar.
		 * @return devuelve el objeto deserializado.
		 * @author Xabi
		 * @date 22/04/2014
		 * @version V1.0		 
		 * */
		ByteArrayInputStream in=new ByteArrayInputStream(data);
		ObjectInputStream is=new ObjectInputStream(in);
		return is.readObject();
	}
}
