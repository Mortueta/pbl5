import java.io.IOException;
import java.net.Socket;


/**

@link HiloEnviar @endlink 
@link GestorSockets @endlink 
@link HiloRecibir @endlink 
@link CalculadorRuta @endlink 
@link Buffer @endlink 
@date 21/04/2014
@author Mikel y
*/



public class Principal {
	private Socket socket;
	private GestorSockets gestorSockets;
	private HiloEnviar hiloEnviar;
	private HiloRecibir hiloRecibir;
	
	private CalculadorRuta calculadorRuta;
	
	private Buffer buffer;
	private boolean booleano = true;
	

	
	public Principal(){

		calculadorRuta=new CalculadorRuta();
		buffer=new Buffer();
		gestorSockets=new GestorSockets();
		socket=gestorSockets.conectarse(socket);
		hiloEnviar=new HiloEnviar(gestorSockets.obtenerFlujoSalida(socket),calculadorRuta,buffer);
		hiloRecibir=new HiloRecibir(gestorSockets.obtenerFlujoEntrada(socket),calculadorRuta,buffer, this);
		
		hiloEnviar.start();
		hiloRecibir.start();
		//System.out.println("Programa listo para enviar");
		//System.out.println("Programa listo para recibir");
		
		while(booleano){
			
		}
	}
	public void cerrarConexion(){
		
		/** 
		 * @brief funci�n que cierra la conexi�n.
		 * @author Mikel
		 * @date 21/04/2014
		 * @version V1.0		 
		 * */

		
		try {
			socket.close();
			//System.out.println("conexion cerrada");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			RegistroFallos.registrarFallo(e.getMessage());
			
		}
	}
	

	public static void main(String[] args){
		Principal principal = new Principal();
	}
}
