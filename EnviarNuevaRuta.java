import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;


public class EnviarNuevaRuta implements Serializable{
	
	/**
	 * Clase EnviarNuevaRuta
	 * 
	 * @brief Esta clase guarda la nueva ruta de un tren que la solicitó previamente
	 * 
	 * @author Josu
	 * 
	 * @date 21/04/2014
	 * 
	 * 
	 */
	
	private int trenId=(Integer) null;
	private int[] viasId=null; 
	
	public EnviarNuevaRuta(int trenId, int[] realizar){
		
		this.trenId = trenId;
		this.viasId = Arrays.copyOf(realizar, realizar.length);
	}
	
	public int getTrenId() {
		
		/**
		 * @return Devuelve el id del tren al que pertenece esta ruta
		 */
		
		return trenId;
	}

	public void setTrenId(int trenId) {
		
		/**
		 * @param Id del tren al que pertenecerá esta ruta
		 */
		
		this.trenId = trenId;
	}

	public int[] getRuta() {
		
		/**
		 * @return Devuelve un array de ints con las ids de las vias
		 * 		   que componen la nueva ruta
		 */
		
		return viasId;
	}

	public void setRuta(ArrayList<Via> ruta) {
		/**
		 * @param Array de ints con las ids que de las vias que componen
		 * 		  la nueva ruta
		 */
		
		this.viasId = viasId;
	}

	public int[] getViasId() {
		
		/**
		 * @return Devuelve un array de ints con las ids de las vias
		 * 		   que componen la nueva ruta
		 */
		
		return viasId;
	}

	public void setViasId(int[] realizar) {
		
		/**
		 * @param Array de ints con las ids que de las vias que componen
		 * 		  la nueva ruta
		 */
		
		this.viasId = Arrays.copyOf(realizar, realizar.length);
	}
}
