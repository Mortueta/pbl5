import java.util.concurrent.Semaphore;


public class Buffer {
	
	/**
	 * Clase Buffer
	 * 
	 * @brief Esta clase es un "almacen" que guarda la ruta que posteriormente se enviara al tren
	 * 		  que haya sufrido alguna incidencia. Guarda, como mucho, una ruta.
	 * 
	 * @author Josu
	 * 
	 * @date 23/04/2014
	 */
	
	private Semaphore semaforoEnviar=null;
	private EnviarNuevaRuta enviarNuevaRuta=null;
	
	public Buffer(){
		
		semaforoEnviar = new Semaphore(0);
	}
	
	public void establecerEnviarNuevaRuta(EnviarNuevaRuta enviarNuevaRuta){
		/**
		 * @param Ruta que se va a almacenar
		 */
		
		this.enviarNuevaRuta = enviarNuevaRuta;
	}
	
	public EnviarNuevaRuta getEnviarNuevaRuta() {
		/**
		 * @return Ruta almacenada previamente
		 */
		
		return enviarNuevaRuta;
	}
	
	public void acquirir(){
		/**
		 * @brief Resta 1 al contador del semaforo empleado para la sincronizacion
		 * 
		 * @autor Josu
		 * 
		 * @date 23/04/2014
		 * 
		 * @version 1.0
		 *
		 */
		
		try {
			semaforoEnviar.acquire();
		} catch (InterruptedException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		}
	}
	
	public void liberar(){
		/**
		 * @brief Suma 1 al contador del semaforo empleado para la sincronizacion
		 * 
		 * @autor Josu
		 * 
		 * @date 23/04/2014
		 * 
		 * @version 1.0
		 *
		 */
		
		semaforoEnviar.release();
	}
	
	public Semaphore getSemaforoEnviar() {
		/**
		 * @return Devuelve el semaforo enviado para lograr la sincronizacion
		 */
		
		return semaforoEnviar;
	}

	public void setSemaforoEnviar(Semaphore semaforoEnviar) {
		/**
		 * @param Semaforo que va a controlar la sincronizacion en esta clase
		 */
		
		this.semaforoEnviar = semaforoEnviar;
	}
	
}
