import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * 
 * @author Xabier Legarda
 * 
 * @brief Gestiona las comunicaciones mediante sockets que el equipo central tiene cuando 
 * 		  un nuevo cliente se conecta.
 * 
 * @date 22/04/2014
 */

public class GestorSockets {
	private final int PUERTO = 5000;
	private BufferedInputStream bis = null;
	private ObjectInputStream ois = null;
	private ServerSocket servidorEquipoCentral = null;
	
	public GestorSockets(){
		
	}
	public Socket aceptarConexion(Socket cliente){
		/** 
		 * @brief acepta las conexi�n al equipo central.
		 * @param recibe el socket del cliente que se quiere conectar
		 * @return devuelve el socket una vez aceptada la conexi�n.
		 * @author Xabi
		 * @date 22/04/2014
		 * @version V1.0		 
		 * */
		try {
			servidorEquipoCentral=new ServerSocket(PUERTO);
			cliente=servidorEquipoCentral.accept();
		} catch (UnknownHostException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		} catch (IOException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		}
		return cliente;
	}
	public ObjectOutputStream obtenerFlujoSalida(Socket cliente){
		/** 
		 * @brief obitene el flujo de salida
		 * @param recibe el socket del cliente que se quiere conectar
		 * @return devuelve el stream de salida con el flujo de salida.
		 * @author Xabi
		 * @date 22/04/2014
		 * @version V1.0		 
		 * */
		OutputStream os = null;
		BufferedOutputStream bos = null;
		ObjectOutputStream oos = null;
		try {
			os=cliente.getOutputStream();
			bos=new BufferedOutputStream(os);
			oos=new ObjectOutputStream(bos);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return oos;
	}
	public ObjectInputStream obtenerFlujoEntrada(Socket cliente){
		/** 
		 * @brief obtiene el flujo de entrada.
		 * @param recibe el socket del cliente que se quiere conectar
		 * @return devuelve el objeto de entrada del stream con el flujo de entrada.
		 * @author Xabi
		 * @date 22/04/2014
		 * @version V1.0		 
		 * */
		InputStream is = null;
		BufferedInputStream bis = null;
		ObjectInputStream ois=null;
		
		try {
			is=cliente.getInputStream();
			bis=new BufferedInputStream(is);
			ois=new ObjectInputStream(bis);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			RegistroFallos.registrarFallos(e.getMessage());
		}
		return ois;
	}
}
