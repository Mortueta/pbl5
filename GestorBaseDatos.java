import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class GestorBaseDatos {
	
	/**
	 * Clase GestorBaseDatos
	 * 
	 * @brief La clase GestorBaseDatos realiza la conexi�n con la base de datos,
	 * lleva a cabo las consultas que queramos y, por �ltimo, cierra la conexion
	 * con la base de datos
	 * 
	 * @author Josu
	 * 
	 * @date 21/04/2013
	 */

	final static String DRIVER = "org.postgresql.Driver";
	final static String CONNECT_STRING = "jdbc:postgresql://127.0.0.1:5432/POPBL5";
	final static String USUARIO = "postgres";
	final static String CONTRASE�A = "1234";
	Connection con=null;
	
	public GestorBaseDatos(){
		
		try {
			Class.forName(DRIVER);
			con = DriverManager.getConnection(CONNECT_STRING, USUARIO, CONTRASE�A);
		} catch (ClassNotFoundException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		} catch (SQLException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		}
	}
	
	public void cerrarConexion(){
		
		/**
		 * @brief Cierra la conexi�n con la base de datos
		 * 
		 * @author Josu
		 * 
		 * @date 21/04/2013
		 * 
		 * @version 1.0
		 * 
		 * @return No devuelve nada
		 * 
		 */
		
		try {
			con.close();
		} catch (SQLException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		}
	}
	
	public ResultSet consultar(String consulta){
		
		/**
		 * @brief Realiza la consulta guardada en la variable "consulta"
		 * 
		 * @author Josu
		 * 
		 * @date 21/04/2013
		 * 
		 * @version 1.0
		 * 
		 * @param consulta guarda el string que sera ejecutado por el sgbd
		 * 		  para realizar la consulta
		 * 
		 * @return ResultSet devuelve el resultado de la consulta almacenado
		 * 		   en una variable de tipo ResultSet
		 * 
		 */
		
		ResultSet ret = null;
		Statement st;
		
		try {
			st = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
									 ResultSet.CONCUR_READ_ONLY);
			ret = st.executeQuery(consulta);
		} catch (SQLException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		}
		
		return ret;
	}
}
