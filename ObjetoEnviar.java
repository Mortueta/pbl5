import java.io.Serializable;

/**@bug se usar� para escribir si algo falla en alg�n caso
@param trenId id del tren que se env�a
@param viaAnterior id de la via por la que ha pasado el tren
@date 21/04/2014
@author Mikel.
*/

public class ObjetoEnviar implements Serializable{
	private int trenId;
	private int viaAnterior;
	
	public ObjetoEnviar(int trenId,int viaAnterior){
		this.trenId=trenId;
		this.viaAnterior=viaAnterior;
	}

	public int getTrenId() {
		return trenId;
	}

	public void setTrenId(int trenId) {
		this.trenId = trenId;
	}

	public int getViaAnterior() {
		return viaAnterior;
	}

	public void setViaAnterior(int viaAnterior) {
		this.viaAnterior = viaAnterior;
	}
}
