
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
@param rs : es la variable en la que se guarda el resultado de la consulta. 
@date 21/04/2014
@author Mikel
*/


public class Via {
	private int id;
	private boolean estado;
	private int coste;
	private int padre;
	
	private int estacionInicio,estacionFinal;
	
	private double total;
	private ResultSet rs;
	
	
	
	public Via(int id,int estacionInicio,int estacionFinal){
		this.id=id;
		this.estacionFinal=estacionFinal;
		this.estacionInicio=estacionInicio;
		this.padre=-1;
		
		this.total=0;
	}
	
	
	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public int getCoste() {
		return coste;
	}

	public void setCoste(int coste) {
		this.coste = coste;
	}

	

	
	

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public int getEstacionInicio() {
		return estacionInicio;
	}

	public void setEstacionInicio(int estacionInicio) {
		this.estacionInicio = estacionInicio;
	}

	public int getEstacionFinal() {
		return estacionFinal;
	}

	public void setEstacionFinal(int estacionFinal) {
		this.estacionFinal = estacionFinal;
	}
	public ArrayList<Integer> calcularHijos(GestorBaseDatos gestor){
		
		/** 
		 * @brief funci�n que sacando desde la BD las vias calcula los hijos de est�
		 * @author Mikel
		 * @date 21/04/2014
		 * @version V1.0 
		 * @see GestorBaseDatos
		 * @return descripci�n del retorno de la funci�n
		 * */

		ArrayList<Integer> hijos = new ArrayList<Integer>();
		
		
		rs=gestor.consultar("select siguiente from unirVias where viaId="+this.id);
		//System.out.println(			"		id consulta "+this.id);
		try {
			rs.beforeFirst();
			while(rs.next()){
				hijos.add(Integer.parseInt(rs.getString(1)));
				//System.out.println("			hijos:" +rs.getString(1));
			}
			rs.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			RegistroFallos.registrarFallo(e.getMessage());
			
		}
		
		
		return hijos;
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPadre() {
		return padre;
	}

	public void setPadre(int padre) {
		this.padre = padre;
	}
	
	
}
