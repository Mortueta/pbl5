import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author Xabier Legarda
 * 
 * @brief Calcula la ruta que el tren debe de seguir.
 * @link GestorBaseDatos
 *
 * @date 27/04/2014
 */
public class CalculadorRuta {
	private GestorBaseDatos gestorBaseDatos =null;
	static ArrayList<Via> vias =null;
	private ArrayList<Via> listaAbierta=new ArrayList<Via>();
	private ArrayList<Via> listaCerrada=new ArrayList<Via>();
	private ArrayList<Integer> hijos=new ArrayList<Integer>();
	private final static double XG = 250;
	private final static double YG = 50;
	private final static double XZ = 550;
	private final static double YZ = 178;
	private final static double XCZ = 250;
	private final static double YCZ = 600; 
	private final static double XCS = 50;
	private final static double YCS = 550;
	private final static double XS = 75;
	private final static double YS = 250;
	private final static double XB = 150;
	private final static double YB = 125;
	private final static double XM = 260;
	private final static double YM = 245;
	private int i = 0;
	private double posicionO [] = null;
	private double posicionD[] = null;
	private double xG = XG;
	private double yG = YG;
	private double xZ = XZ;
	private double yZ = YZ;
	private double xCz = XCZ;
	private double yCz = YCZ;
	private double xCs = XCS;
	private double yCs = YCS;
	private double xS = XS;
	private double yS = YS;
	private double xB = XB;
	private double yB = YB;
	private double xM = XM;
	private double yM = YM;
	
	public CalculadorRuta(){
		vias = new ArrayList<Via>();
		gestorBaseDatos=new GestorBaseDatos();
		try {
			iniciarVias();
		} catch (SQLException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		}
	}
	
	public void iniciarVias() throws SQLException{
		/** 
		 * @brief funcion que inicializa las vias.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		ResultSet rs = null;
		int posicion=0;
		rs=gestorBaseDatos.consultar("select * from vias order by viaId");
		rs.beforeFirst();
		while(rs.next()){
			vias.add(new Via(Integer.parseInt(rs.getString(1)),Integer.parseInt(rs.getString(5)
					),Integer.parseInt(rs.getString(6))));
		}
		
		rs=gestorBaseDatos.consultar("select viaid,tiempo from vias join rutas on " +
									 "vias.rutaid=rutas.rutaid order by viaid");
		rs.beforeFirst();
		while(rs.next()){
			vias.get(posicion).setCoste(Integer.parseInt(rs.getString(1)));
			posicion++;
		}
	}

	public int getDestino(int trenId){
		/** 
		 * @brief funci�n que permite coger el destiono
		 * @param recibe el id del tren
		 * @return devuelve el destino
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		int viajeId= 0;
		int tipoViajeId = 0;
		int destino = -1;
		int viaId = 0;
		try {
			viajeId=obtenerViajeId(trenId);
			viaId=obtenerViaId(trenId);
			tipoViajeId=obtenerTipoViajeId(viajeId);
			destino=obtenerDestino(tipoViajeId);
		} catch (SQLException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		}
		return destino;
	}
	
	public void algoritmoBusqueda(Via via,int destino){
		/** 
		 * @brief funci�n que busca la ruta alternativa de la via que se le pasa teneidno un destino determinado.
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		if(via.getId()>=13 && via.getId()<=17){
			if(via.getEstacionInicio()==destino){
				listaCerrada.add(via);
				arreglar();
			
			}else{
				noEstaEnInicio(via, destino);
			}
		}else{
			esUniDireccional(via, destino);
		}
	}
	
	public void comprobarEstadoViaHijo(Via via, int destino){
		/** 
		 * @brief funci�n que comprueba el estado (si es transitable) de los hijos (vias directamente conetcadas) de la v�a actual. 
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		if((comprobarEstadoVia(vias.get(hijos.get(i)-1)))&& 
		   (comprobarSiEstaEnListaCerrada(vias.get(hijos.get(i)-1))) && 
		   (comprobarSiHijoValido(via,vias.get(hijos.get(i)-1))) && 
		   (comprobarSiEstaEnListaAbierta(vias.get(hijos.get(i)-1)))){
				vias.get(hijos.get(i)-1).setPadre(via.getId());
				listaAbierta.add(vias.get(hijos.get(i)-1));
		}
	}
	
	public void comprobarItemUniDireccional(Via via, int destino){
		/** 
		 * @brief comprueba la via unidireccional y si ha acabado saca el coste final de viaje..
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		if(listaAbierta.get(i).getEstacionInicio()==destino){
			listaAbierta.get(i).setTotal(via.getCoste()+listaAbierta.get(i).getCoste());	
		}else{
			posicionO=sacarCoordenadas(listaAbierta.get(i).getEstacionFinal());
			double c1=listaAbierta.get(i).getCoste()+calcularDistancia(posicionO[0],
					  posicionO[1],posicionD[0],posicionD[1]);
			listaAbierta.get(i).setTotal(c1);
		}
	}
	
	public void comprobarItemBiDireccional(Via via, int destino){
		/** 
		 * @brief comprueba la via unidireccional y si ha acabado saca el coste final de viaje..
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		if(listaAbierta.get(i).getEstacionFinal()==destino){
			listaAbierta.get(i).setTotal(via.getCoste()+listaAbierta.get(i).getCoste());
		}else{
			posicionO=sacarCoordenadas(listaAbierta.get(i).getEstacionInicio());
			double c1=listaAbierta.get(i).getCoste()+calcularDistancia(posicionO[0],
					  posicionO[1],posicionD[0],posicionD[1]);
			listaAbierta.get(i).setTotal(c1);
		}
	}
	
	public void uniDireccionalNotDestino(Via via, int destino){
		/** 
		 * @brief realiza la comprobaci�n de la v�a unidireccional si es el destino.
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		if(comprobarSiEstaEnListaCerrada(via)){
			listaCerrada.add(via);
		}
		if(via.getPadre()!=-1){
			via.setCoste(via.getCoste()+vias.get(via.getPadre()-1).getCoste());
		}
		hijos=via.calcularHijos(gestorBaseDatos);
		for(i=0;i<hijos.size();i++){
			comprobarEstadoViaHijo(via, destino);
		}
		posicionD=sacarCoordenadas(destino);
		for(i=0;i<listaAbierta.size();i++){
			if(listaAbierta.get(i).getId()>=13 || listaAbierta.get(i).getId()<=17){
				comprobarItemUniDireccional(via, destino);
			}else{
				comprobarItemBiDireccional(via, destino);
			}
			
		}
		buscarListaEnMatriz(via, destino);
		algoritmoBusqueda(listaAbierta.remove(0),destino);
	}
	
	public void buscarListaEnMatriz(Via via, int destino){
		
		/** 
		 * @brief busca la lista desea en la matriz.
		 * @param recibe la via y el destino de la lista interesada.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		int contador= 0;
		int contadorFor2= 0;
		for(contador=0;contador<listaAbierta.size()-1;contador++){
			for(contadorFor2=0;contadorFor2<listaAbierta.size()-1;contadorFor2++){
				if(listaAbierta.get(contadorFor2).getTotal()>
				   listaAbierta.get(contadorFor2+1).getTotal()){
					Via aux=listaAbierta.get(contadorFor2);
					listaAbierta.set(contadorFor2, listaAbierta.get(contadorFor2+1));
					listaAbierta.set(contadorFor2+1, aux);
				}
			}
		}
	}
	
	public void esUniDireccional(Via via, int destino){
		
		/** 
		 * @brief Comprueba que la via sea de tipo unidireccional.
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0	
		 * */
		if(via.getEstacionFinal()==destino){
			listaCerrada.add(via);
			arreglar();
			imprimirListaCerrada();
		}else{
			uniDireccionalNotDestino(via, destino);
		}			
	}
	
	public void recorrerListaUniDireccional(Via via, int destino){
		
		/** 
		 * @brief Recorre las vias unidireccionales(las que van a madrid) y calcula el coste.
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0	 
		 * */
		if(listaAbierta.get(i).getEstacionInicio()==destino){
			listaAbierta.get(i).setTotal(via.getCoste()+listaAbierta.get(i).getCoste());
		}else{
			posicionO=sacarCoordenadas(listaAbierta.get(i).getEstacionFinal());
			double c1=listaAbierta.get(i).getCoste()+calcularDistancia(posicionO[0],
					  posicionO[1],posicionD[0],posicionD[1]);
			listaAbierta.get(i).setTotal(c1);
		}
	}
	public void recorrerListaBiDireccional(Via via, int destino){
		
		/** 
		 * @brief Recorre las vias bidireccionales  y calcula el coste.
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0	
		 
		 * */
		if(listaAbierta.get(i).getEstacionFinal()==destino){
			listaAbierta.get(i).setTotal(via.getCoste()+listaAbierta.get(i).getCoste());
		}else{
			posicionO=sacarCoordenadas(listaAbierta.get(i).getEstacionInicio());
			double c1=listaAbierta.get(i).getCoste()+calcularDistancia(posicionO[0],
					  posicionO[1],posicionD[0],posicionD[1]);
			listaAbierta.get(i).setTotal(c1);
		}
	}
	public void recorrerLista(Via via, int destino){
		
		/** 
		 * @brief Recorre todas las v�as que estan en la lista.
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0	
		 * @see recorrerListaBiDireccional(Via via, int destino)
		 * @see recorrerListaUniDireccional(Via via, int destino)	 
		 * */
		int contador=0;
		int contadorFor2 = 0;
		for(contador=0;contador<listaAbierta.size();contador++){
			if(listaAbierta.get(contador).getId()>=13 || 
			   listaAbierta.get(contador).getId()<=17){
				recorrerListaUniDireccional(via, destino);
			}else{
				recorrerListaBiDireccional(via, destino);
			}
		}
		for(contador=0;contador<listaAbierta.size()-1;contador++){
			for(contadorFor2=0;contadorFor2<listaAbierta.size()-1;contadorFor2++){
				if(listaAbierta.get(contadorFor2).getTotal()>
				   listaAbierta.get(contadorFor2+1).getTotal()){
					Via aux=listaAbierta.get(contadorFor2);
					listaAbierta.set(contadorFor2, listaAbierta.get(contadorFor2+1));
					listaAbierta.set(contadorFor2+1, aux);
				}
			}
		}
	}
	
	public void comprobarViasHijos(Via via, int destino){
		
		/** 
		 * @brief Comprueba que los hijso disponibles esten disponibles y lo a�ade a la lista abierta( vias disponibles)
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0	
		 * @see recorrerListaBiDireccional(Via via, int destino)
		 * @see recorrerListaUniDireccional(Via via, int destino)	 
		 * */
		int contador=0;
		
		for(contador = 0;contador<hijos.size();contador++){
			if((comprobarEstadoVia(vias.get(hijos.get(contador)-1))) && 
			   (comprobarSiEstaEnListaCerrada(vias.get(hijos.get(contador)-1))) && 
			   (comprobarSiHijoValido(via,vias.get(hijos.get(contador)-1))) &&
			   (comprobarSiEstaEnListaAbierta(vias.get(hijos.get(contador)-1)))){
				vias.get(hijos.get(contador)-1).setPadre(via.getId());
				listaAbierta.add(vias.get(hijos.get(contador)-1));
			}
			
		}
	}
	
	public void noEstaEnInicio(Via via, int destino){
		
		/** 
		 * @brief comprueba la via unidireccional y si ha acabado saca el coste final de viaje..
		 * @author Mikel
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		if(comprobarSiEstaEnListaCerrada(via)){
			listaCerrada.add(via);
		}
		if(via.getPadre()!=-1){
			via.setCoste(via.getCoste()+vias.get(via.getPadre()-1).getCoste());
		}
		hijos=via.calcularHijos(gestorBaseDatos);
		comprobarViasHijos(via, destino);
		posicionD=sacarCoordenadas(destino);
		recorrerLista(via, destino);
		algoritmoBusqueda(listaAbierta.remove(0),destino);
	}
	
	public ArrayList<Via> getListaCerrada() {
		return listaCerrada;
	}
	
	public void reestablecer(){
		/** 
		 * @brief funci�n reestablece las listas a inicio.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		listaAbierta.clear();
		listaCerrada.clear();
		vias.clear();
		try {
			iniciarVias();
		} catch (SQLException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		}
	}
	
	public double calcularDistancia(double xActual,double yActual,double xDestino,
									double yDestino){
		/** 
		 * @brief calcula la distancia entre dos puntos
		 * @param recibe las coordenadas de dos puntos
		 * @return devuelve la distancia
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		double total=0;
		double valorX = xDestino-xActual;
		double valorY = yDestino-yActual;
		double valorX2 = Math.pow(valorX, 2);
		double valorY2 = Math.pow(valorY, 2);
		total = Math.sqrt(valorX2+valorY2);
		return total;	
	}
	
	public double[] sacarCoordenadas(int e){
		double posicionT[] = new double[2];
		switch(e){
		case 3:
			posicionT[0]=xM;
			posicionT[1]=yM;
			break;
		case 2:
			posicionT[0]=xB;
			posicionT[1]=yB;
			break;
		case 1:
			posicionT[0]=xG;
			posicionT[1]=yG;
			break;
		case 7:
			posicionT[0]=xCz;
			posicionT[1]=yCz;
			break;
		case 6:
			posicionT[0]=xCs;
			posicionT[1]=yCs;
			break;
		case 5:
			posicionT[0]=xS;
			posicionT[1]=yS;
			break;
		case 4:
			posicionT[0]=xZ;
			posicionT[1]=yZ;
			break;
		
		}
		return posicionT;
	}
	
	public boolean comprobarSiHijoValido(Via via,Via hijo){
		
		/** 
		 * @brief comprueba si un hijo es v�lido.
		 * @param recibe la via y el hijo a comprobar
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		boolean resultado = true;
		if(via.getId()>=13 && via.getPadre()%2!=0 && hijo.getId()==(via.getPadre()+1)){
			resultado = false;
		}
		if(via.getId()>=13 && via.getPadre()%2==0 && hijo.getId()==(via.getPadre()-1)){
			resultado = false;
		}
		return resultado;
	}
	
	public void imprimirListaAbierta(){
		
		/** 
		 * @brief permita hacer el seguimiento de la lista abierta.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		String seguimiento = null;
		seguimiento = "*************** LISTA ABIERTA";
		for(int i=0;i<listaAbierta.size();i++){
			seguimiento += "posicion "+i+" id via : "+listaAbierta.get(i).getId();
		}
		seguimiento = "***************";
		RegistroFallos.registrarSeguimiento(seguimiento);
	}
	
	public void imprimirListaCerrada(){
		
		/** 
		 * @brief permita hacer el seguimiento de la lista cerrada.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		String seguimiento = null;
		seguimiento = "*************** LISTA CERRADA";
		for(int i=0;i<listaCerrada.size();i++){
			seguimiento = "VIA: "+listaCerrada.get(i).getId()+" PADRE "
					      +listaCerrada.get(i).getPadre();
			
		}
		seguimiento = "***************";
		RegistroFallos.registrarSeguimiento(seguimiento);
	}
	
	public boolean comprobarSiEstaEnListaCerrada(Via via){
		
		/** 
		 * @brief comprueba si una via esta en la lista cerrada.
		 * @param recibe la via a comprobar.
		 * @return devuelve si est� o no en la lista.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		boolean resultado = true;
		int contador=0;
		for(contador=0;contador<listaCerrada.size();contador++){
			if(via.getId()==listaCerrada.get(contador).getId()){
				resultado = false;
			}
		}
		return resultado;
	}
	
	public boolean comprobarSiEstaEnListaAbierta(Via via){
		
		/** 
		 * @brief comprueba si una via esta en la lista abierta.
		 * @param recibe la via a comprobar.
		 * @return devuelve si est� o no en la lista.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		int contador = 0;
		for(contador=0;contador<listaAbierta.size();contador++){
			if(via.getId()==listaAbierta.get(contador).getId()){
				return false;
			}
		}
		return true;
	}
	
	public boolean comprobarEstadoVia(Via via){
		/** 
		 * @brief comprueba si una via esta cerrada o abierta.
		 * @param recibe la via a comprobar.
		 * @return devuelve si est� abierta o cerrada.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		int id = 0;
		ResultSet rs = null;
		id=via.getId();
		rs=gestorBaseDatos.consultar("select estado from vias where viaId="+id);
		try {
			rs.beforeFirst();
			rs.next();
			if(rs.getString(1).equals("t")){
				return true;
			}
		} catch (SQLException e) {
			RegistroFallos.registrarFallos(e.getMessage());
		}
		return false;
	}
	
	public int obtenerViajeId(int trenId) throws SQLException{
		
		/** 
		 * @brief obitene el id del viaje por el que circula el tren.
		 * @param recibe el tren que realiza el viaje.
		 * @return devuelve id del viaje por el circula el tren.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		ResultSet rs = null;
		
		rs=gestorBaseDatos.consultar("select viajeid from viajes where viajes.trenId="+trenId);
		rs.next();
		return Integer.parseInt(rs.getString(1));
		
	}
	
	public int obtenerViaId(int trenId) throws SQLException{
		
		/** 
		 * @brief obitene el id del via por el que circula el tren.
		 * @param recibe el tren que realiza el via.
		 * @return devuelve id del via por el circula el tren.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		ResultSet rs = null;
		rs=gestorBaseDatos.consultar("select viaAnteriorId from trenes where trenId="+trenId);
		rs.next();
		return Integer.parseInt(rs.getString(1));
		
	}
	
	public int obtenerTipoViajeId(int viajeId) throws SQLException{
		/** 
		 * @brief obitene el id del tipo de viaje por el que circula el tren.
		 * @param recibe el tren que realiza el viaje.
		 * @return devuelve id del v tipo de viaje por el circula el tren.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		ResultSet rs = null;
		
		rs=gestorBaseDatos.consultar("select tipoviajeid from viajes join tiposdeviaje " +
									 "on viajes.tipoViaje=tiposdeviaje.tipoviajeid " +
									 "where viajeid="+viajeId);
		rs.next();
		return Integer.parseInt(rs.getString(1));
	}
	
	public int obtenerDestino(int tipoViajeId) throws SQLException{
		
		/** 
		 * @brief obitene el destino de un tipo de viaje
		 * @param recibe el id de un tipo de viaje.
		 * @return devuelve el destino del tipo de viaje recibido.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		ResultSet rs = null;
		
		rs=gestorBaseDatos.consultar("select destino from tiposdeviaje " +
									 "where tipoviajeid="+tipoViajeId);
		rs.next();
		return Integer.parseInt(rs.getString(1));
	}
	
	public Via getVia(int numVia){
		return vias.get(numVia-1);
	}
	
	public  void arreglar(){
		/** 
		 * @brief arregla el elemento seleccionado que no funciona.
		 * @author Xabi
		 * @date 26/04/2014
		 * @version V1.0		 
		 * */
		int contador = 0;
		int contadorFor2 = 0;
		boolean borrar=true;
		ArrayList<Integer> padres=new ArrayList<Integer>();
		for(contador=0;contador<listaCerrada.size();contador++){
			padres.add(listaCerrada.get(contador).getPadre());
		}
		for(contador=0;contador<listaCerrada.size();contador++){
			borrar=true;
			for(contadorFor2=0;contadorFor2<padres.size();contadorFor2++){
				if(listaCerrada.get(contador).getId()==padres.get(contadorFor2)){
					borrar=false;
					break;
				}else{
					borrar=true;
				}
			}
			if(borrar==true && contador!=listaCerrada.size()-1){
				listaCerrada.remove(contador);
			}
		}
		listaCerrada.remove(0);
	}
}
