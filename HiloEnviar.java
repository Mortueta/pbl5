import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 * 
 * @author Xabier Legarda
 * 
 * @brief Env�a al comunicador que conecta el Equipo Central con el 
 * 		  Equipo de Simulaci�n.
 * @link CalculadorRuta
 * @link EnviarNuevaRuta
 * 
 * @date 22/04/2014
 */

public class HiloEnviar extends Thread{
	private ObjectOutputStream oos = null;
	private CalculadorRuta calculadorRuta = null;
	private Buffer buffer = null;
	
	public HiloEnviar(ObjectOutputStream oos,CalculadorRuta calculadorRuta,Buffer buffer){
		this.oos=oos;
		this.calculadorRuta=calculadorRuta;
		this.buffer=buffer;
	}
	
	public void run(){
		/** 
		 * @brief funci�n que define las acciones que debe realizar el hilo.
		 * @author Xabi
		 * @date 22/04/2014
		 * @version V1.0		 
		 * */
		EnviarNuevaRuta enviarNuevaRuta = null;
		int [] realizarNuevaRuta = null;
		int contador=0;
		boolean continuar = true;
		String seguimiento = null;
		while(continuar){
			
			buffer.acquirir();
			enviarNuevaRuta=buffer.getEnviarNuevaRuta();
			realizarNuevaRuta=enviarNuevaRuta.getRuta();
			
			for(contador = 0;contador<realizarNuevaRuta.length;contador++){
				seguimiento = "via "+contador+" = "+realizarNuevaRuta[contador];
				RegistroFallos.registrarSeguimiento(seguimiento);
			}
			seguimiento += "para el tren "+enviarNuevaRuta.getTrenId();
			RegistroFallos.registrarSeguimiento(seguimiento);
			
			try {
				oos.writeObject(enviarNuevaRuta);
				oos.flush();
			} catch (IOException e) {
				RegistroFallos.registrarFallos(e.getMessage());
			}
		}
		
		
	}

}
